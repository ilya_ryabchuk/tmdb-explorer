package com.iliarb.tmdbexplorer.data.model.tv_show

import com.iliarb.tmdbexplorer.data.entity.TvShow
import com.iliarb.tmdbexplorer.data.model.BaseModel
import com.iliarb.tmdbexplorer.data.network.ApiConfig
import com.iliarb.tmdbexplorer.data.network.response.ListResponse
import com.iliarb.tmdbexplorer.util.extensions.applySchedulers
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

/**
 * @author Ilya Ryabchuk on 06.11.16.
 */
class TvShowModel @Inject constructor(tvShowService: TvShowService) : BaseModel() {

    private val service: TvShowService = tvShowService

    fun getTvShows(type: String, page: Int): Single<MutableList<TvShow>> {
        return service.getTvShows(type, page)
                .applySchedulers()
                .compose(transformException())
                .map(ListResponse<TvShow>::results)
    }

    fun getTvShowDetails(id: Int): Observable<TvShow> {
        return service.getTvShowDetails(id, ApiConfig.APPEND_TV_SHOW_DETAILS).applySchedulers()
    }

}