package com.iliarb.tmdbexplorer.data.model.movie

import com.iliarb.tmdbexplorer.data.entity.Movie
import com.iliarb.tmdbexplorer.data.entity.Video
import com.iliarb.tmdbexplorer.data.network.response.CreditsResponse
import com.iliarb.tmdbexplorer.data.network.response.ListResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * @author Ilya Ryabchuk on 22.10.16.
 */
interface MovieService {

    @GET("movie/{type}")
    fun getMovies(@Path("type") path: String,
                  @Query("page") page: Int): Single<ListResponse<Movie>>

    @GET("movie/{id}")
    fun getMovieDetails(@Path("id") id: Int,
                        @Query("append_to_response") append: String): Single<Movie>

    @GET("movie/{id}/credits")
    fun getMovieCredits(@Path("id") id: Int): Observable<CreditsResponse>

    @GET("movie/{id}/videos")
    fun getMovieVideos(@Path("id") id: Int): Observable<ListResponse<Video>>

}