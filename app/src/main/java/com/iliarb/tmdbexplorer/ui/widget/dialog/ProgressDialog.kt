package com.iliarb.tmdbexplorer.ui.widget.dialog

import android.app.Dialog
import android.app.DialogFragment
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.*
import com.iliarb.tmdbexplorer.R

/**
 * @author ilya on 3/14/17.
 */
class ProgressDialog : DialogFragment() {

    companion object {

        private const val ARG_MESSAGE = "message"

        fun newInstance(message: String?): ProgressDialog {
            val dialog = ProgressDialog()
            val args = Bundle()

            args.putString(ARG_MESSAGE, message)
            dialog.arguments = args

            return dialog
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        if (dialog.window != null) {
            dialog.window.requestFeature(Window.FEATURE_NO_TITLE)
        }
        return dialog
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val window = dialog.window
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            window.attributes.windowAnimations = 0
            window.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.color.dialog_progress_background))

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                window.setFlags(
                        WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                        WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater?,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater?.inflate(R.layout.dialog_progress, container, false)

    fun isShowing(): Boolean = dialog != null && dialog.isShowing
}