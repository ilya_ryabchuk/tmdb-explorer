package com.iliarb.tmdbexplorer.data.network.response

/**
 * @author Ilya Ryabchuk on 27.11.16.
 */
data class RequestTokenResponse(
        val success: Boolean,
        val expiresAt: String,
        val requestToken: String)