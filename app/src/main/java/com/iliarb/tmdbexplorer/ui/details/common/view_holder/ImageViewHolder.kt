package com.iliarb.tmdbexplorer.ui.details.common.view_holder

import android.view.View
import com.bumptech.glide.Glide
import com.iliarb.tmdbexplorer.data.entity.Backdrop
import com.iliarb.tmdbexplorer.ui.base.recyclerview.BaseViewHolder
import com.iliarb.tmdbexplorer.util.extensions.loadImage
import kotlinx.android.synthetic.main.item_image.view.*

/**
 * @author Ilya Ryabchuk on 30.03.17.
 */
class ImageViewHolder(view: View, clickListener: (Int) -> Unit) : BaseViewHolder<Backdrop>(view) {

    init {
        itemView.setOnClickListener { clickListener.invoke(adapterPosition) }
    }

    override fun bind(item: Backdrop, position: Int) {
        itemView.itemImage.loadImage(item.filePath)
    }

    override fun prepareForReuse() = Glide.clear(itemView.itemImage)
}