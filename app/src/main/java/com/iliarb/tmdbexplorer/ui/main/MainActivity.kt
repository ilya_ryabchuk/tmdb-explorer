package com.iliarb.tmdbexplorer.ui.main

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.design.widget.TabLayout.OnTabSelectedListener
import android.support.design.widget.TabLayout.Tab
import android.view.Menu
import android.view.MenuItem
import com.iliarb.tmdbexplorer.R
import com.iliarb.tmdbexplorer.data.Preferences
import com.iliarb.tmdbexplorer.ui.account.AccountActivity
import com.iliarb.tmdbexplorer.ui.auth.AuthActivity
import com.iliarb.tmdbexplorer.ui.base.Presenter
import com.iliarb.tmdbexplorer.ui.base.inject.BaseInjectActivity
import com.iliarb.tmdbexplorer.util.extensions.initActionBar
import com.iliarb.tmdbexplorer.util.rx.RxSharedPreferences
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

/**
 * @author Ilya Ryabchuk on 22.10.16.
 */
class MainActivity : BaseInjectActivity() {

    @Inject lateinit var viewPagerAdapter: MainPagerAdapter
    @Inject lateinit var preferences: RxSharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        uiComponent.inject(this)

        initActionBar(toolbar)

        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_movie_black_24dp))
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_live_tv_black_24dp))

        tabLayout.addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabReselected(tab: Tab?) {
                viewPager.currentItem = tab?.position!!
            }

            override fun onTabUnselected(tab: Tab?) {

            }

            override fun onTabSelected(tab: Tab?) {
                when (tab?.position) {
                    0 -> toolbar.title = getString(R.string.nav_item_movies)
                    1 -> toolbar.title = getString(R.string.nav_item_tv)
                }

                viewPager.currentItem = tab?.position!!
            }
        })

        viewPager.adapter = viewPagerAdapter
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.action_discover -> true

            R.id.action_account -> {
                if (preferences.has(Preferences.KEY_SESSION_ID)) {
                    AccountActivity.getRouteIntent().startFrom(this)
                } else {
                    AuthActivity.getRouteIntent().startFrom(this)
                }
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun getPresenter(): Presenter<*>? = null
}