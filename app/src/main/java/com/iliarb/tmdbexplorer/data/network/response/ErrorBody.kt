package com.iliarb.tmdbexplorer.data.network.response

/**
 * @author ilya on 3/14/17.
 */
data class ErrorBody(val statusMessage: String, val success: Boolean, val statusCode: Int)