package com.iliarb.tmdbexplorer.ui.auth

import com.iliarb.tmdbexplorer.ui.base.MvpView

/**
 * @author Ilya Ryabchuk on 27.11.16.
 */
interface AuthView : MvpView {

    fun onLoginSuccess()

}