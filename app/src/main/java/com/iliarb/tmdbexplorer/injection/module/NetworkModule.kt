package com.iliarb.tmdbexplorer.injection.module

import com.github.simonpercic.oklog3.OkLogInterceptor
import com.google.gson.FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.iliarb.tmdbexplorer.BuildConfig
import com.iliarb.tmdbexplorer.data.model.auth.AccountService
import com.iliarb.tmdbexplorer.data.model.auth.AuthService
import com.iliarb.tmdbexplorer.data.model.movie.MovieService
import com.iliarb.tmdbexplorer.data.model.tv_show.TvShowService
import com.iliarb.tmdbexplorer.data.network.ApiConfig
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit.SECONDS
import javax.inject.Singleton

/**
 * @author Ilya Ryabchuk on 22.10.16.
 */
@Module
class NetworkModule {

    object Connection {

        internal const val CONNECT_TIMEOUT = 10L
        internal const val READ_TIMEOUT = 10L
        internal const val WRITE_TIMEOUT = 10L

    }

    // region Providers
    @Provides
    @Singleton
    fun provideMovieService(): MovieService =
            provideApiClient().create(MovieService::class.java)

    @Provides
    @Singleton
    fun provideTvService(): TvShowService =
            provideApiClient().create(TvShowService::class.java)

    @Provides
    @Singleton
    fun provideAuthService(): AuthService =
            provideApiClient().create(AuthService::class.java)

    @Provides
    @Singleton
    fun provideAccountService(): AccountService =
            provideApiClient().create(AccountService::class.java)

//    @Provides
//    @Singleton
//    fun provideCompanyService() : CompanyService =
//            provideApiClient().create(CompanyService::class.java)


//    @Provides
//    @Singleton
//    fun providePersonService() : PersonService =
//            provideApiClient().create(PersonService::class.java)
//

//
//    @Provides
//    @Singleton
//    fun provideDiscoverService() : DiscoverService =
//            provideApiClient().create(DiscoverService::class.java)
//
//    @Provides
//    @Singleton
//    fun provideSearchService() : SearchService =
//            provideApiClient().create(SearchService::class.java)
//
//    @Provides
//    @Singleton
//    fun provideGooglePlacesService() : GooglePlacesService =
//            provideGooglePlacesClient().create(GooglePlacesService::class.java)
    // endregion

    private fun provideOkHttpClient(): OkHttpClient {
        val builder = OkHttpClient().newBuilder()

        builder.connectTimeout(Connection.CONNECT_TIMEOUT, SECONDS)
                .readTimeout(Connection.READ_TIMEOUT, SECONDS)
                .writeTimeout(Connection.WRITE_TIMEOUT, SECONDS)

        builder.addInterceptor(createTokenInterceptor())

        val requestLogging = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        if (BuildConfig.DEBUG) {
            builder.addInterceptor(OkLogInterceptor.builder()
                    .withRequestHeaders(true)
                    .withRequestBody(true)
                    .build())
                    .addInterceptor(requestLogging)
        }

        return builder.build()
    }

    private fun provideGooglePlacesClient(): Retrofit {
        return Retrofit.Builder()
                .baseUrl(ApiConfig.GOOGLE_PLACES_URL)
                .client(provideOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create(createGsonConverter()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    private fun provideApiClient(): Retrofit {
        return Retrofit.Builder()
                .baseUrl(ApiConfig.API_URL)
                .client(provideOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create(createGsonConverter()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    private fun createGsonConverter(): Gson {
        return GsonBuilder()
                .setFieldNamingPolicy(LOWER_CASE_WITH_UNDERSCORES)
                .create()
    }

    private fun createTokenInterceptor(): Interceptor {
        return Interceptor { chain ->
            var request = chain.request()
            val url = request.url().newBuilder()
                    .addQueryParameter("api_key", BuildConfig.API_KEY)
                    .build()

            request = request.newBuilder().url(url).build()

            return@Interceptor chain.proceed(request)
        }
    }

}