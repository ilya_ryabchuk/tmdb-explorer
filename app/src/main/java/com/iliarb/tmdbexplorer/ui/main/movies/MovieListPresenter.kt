package com.iliarb.tmdbexplorer.ui.main.movies

import com.iliarb.tmdbexplorer.data.entity.Tag
import com.iliarb.tmdbexplorer.data.model.movie.MovieModel
import com.iliarb.tmdbexplorer.ui.base.BasePresenter
import io.reactivex.functions.Consumer
import javax.inject.Inject

/**
 * @author Ilya Ryabchuk on 23.10.16.
 */
class MovieListPresenter @Inject constructor(model: MovieModel, internal val adapter: MoviesAdapter)
    : BasePresenter<MovieListView>() {

    internal var type: String = ""

    private val movieModel: MovieModel = model

    init {
        adapter.callback = { position ->
            mvpView?.showMovieDetails(adapter.get(position))
        }
    }

    internal fun onTagChanged(tag: Tag) {
        type = tag.code
        getMovies(1)
    }

    internal fun getMovies(page: Int) {
        if (type.isNotEmpty()) {
            movieModel.getMovies(type, page).subscribe(Consumer {
                adapter.setList(it)
            }, defaultErrorConsumer)
        }
    }
}