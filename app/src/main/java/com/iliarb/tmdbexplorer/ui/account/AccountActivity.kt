package com.iliarb.tmdbexplorer.ui.account

import android.os.Bundle
import android.support.v7.widget.Toolbar
import com.iliarb.tmdbexplorer.R
import com.iliarb.tmdbexplorer.ui.base.Presenter
import com.iliarb.tmdbexplorer.ui.base.inject.BaseInjectActivity
import com.iliarb.tmdbexplorer.util.Router
import com.iliarb.tmdbexplorer.util.extensions.initActionBar
import kotlinx.android.synthetic.main.activity_account.*
import javax.inject.Inject

/**
 * @author Ilya Ryabchuk on 27.11.16.
 */
class AccountActivity : BaseInjectActivity(), AccountView {

    @Inject lateinit var presenter: AccountPresenter

    companion object {

        fun getRouteIntent(): Router.ActivityRouteBuilder<AccountActivity> {
            return Router.ActivityRouteBuilder(AccountActivity::class.java)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)
        uiComponent.inject(this)

        initActionBar(toolbar,
                addNavigationButton = true,
                menu = R.menu.menu_account,
                menuClickListener = Toolbar.OnMenuItemClickListener {
                    return@OnMenuItemClickListener when (it.itemId) {
                        R.id.action_logout -> {
                            presenter.logout()
                            true
                        }
                        else -> false
                    }
                })

        presenter.attachView(this)
        presenter.getUserDetails()
    }

    override fun showUserDetails(name: String, username: String) {
        accountImage.text = username.toUpperCase().substring(0, 1)
        accountName.text = name
        accountNickname.text = getString(R.string.account_username, username)
    }

    override fun onLoggedOut() = finish()

    override fun getPresenter(): Presenter<*>? = presenter
}