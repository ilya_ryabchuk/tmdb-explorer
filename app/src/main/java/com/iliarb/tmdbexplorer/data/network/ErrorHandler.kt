package com.iliarb.tmdbexplorer.data.network

import android.support.annotation.StringRes
import com.iliarb.tmdbexplorer.R
import com.iliarb.tmdbexplorer.ui.base.MvpView
import io.reactivex.functions.Consumer
import java.lang.ref.WeakReference
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/**
 * @author ilya on 3/14/17.
 */

class ErrorHandler(view: MvpView? = null,
                   private val showError: Boolean = false,
                   private val onFailure: (Throwable, Boolean) -> Unit) : Consumer<Throwable> {

    private val viewRef: WeakReference<MvpView?> = WeakReference(view)

    override fun accept(t: Throwable) {
        var isNetwork = false

        if (isNetworkError(t)) {
            isNetwork = true
            showErrorIfRequired(R.string.error_internet_connection, -1)
        }

        onFailure.invoke(t, isNetwork)
    }

    private fun showErrorIfRequired(@StringRes messageRes: Int, errorCode: Int = -1) {
        if (showError) {
            viewRef.get()?.showMessage(messageRes, errorCode)
        }
    }

    private fun isNetworkError(throwable: Throwable): Boolean {
        return throwable is SocketException
                || throwable is UnknownHostException
                || throwable is SocketTimeoutException
    }
}
