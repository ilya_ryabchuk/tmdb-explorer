package com.iliarb.tmdbexplorer.ui.main

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

/**
 * @author Ilya Ryabchuk on 23.10.16.
 */
class MainViewPager : ViewPager {

    constructor(context: Context) : super(context)

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)

    /**
     * Return false to prevent swipe between tabs
     */
    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean = false

    /**
     * Return false to prevent swipe between tabs
     */
    override fun onTouchEvent(ev: MotionEvent?): Boolean = false

}