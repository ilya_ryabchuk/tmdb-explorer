package com.iliarb.tmdbexplorer.ui.details.movie

import com.iliarb.tmdbexplorer.data.entity.Movie
import com.iliarb.tmdbexplorer.ui.base.MvpView

/**
 * @author ilya on 3/19/17.
 */
interface MovieDetailsView : MvpView {

    fun showMovieDetails(movie: Movie)

}