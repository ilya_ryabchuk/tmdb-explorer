package com.iliarb.tmdbexplorer.data.entity

import com.iliarb.tmdbexplorer.data.network.response.ListResponse

/**
 * @author Ilya Ryabchuk on 27.10.16.
 */
data class Company(
        val id: Int,
        val name: String,
        val logoPath: String?,
        val headquarters: String?,
        val homepage: String?,
        val movies: ListResponse<Movie>,
        val description: String?)