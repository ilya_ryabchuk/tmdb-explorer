package com.iliarb.tmdbexplorer.data.network

import android.support.annotation.StringDef

/**
 * @author ilya on 3/11/17.
 */
object ApiConfig {

    const val API_URL = "https://api.themoviedb.org/3/"
    const val IMAGE_URL = "https://image.tmdb.org/t/p/w500"

    const val APPEND_MOVIE_DETAILS = "keywords,images,credits"
    const val APPEND_COMPANY_DETAILS = "movies"
    const val APPEND_TV_SHOW_DETAILS = "images,seasons,credits"
    const val APPEND_PERSON_DETAILS = "images"

    const val GOOGLE_PLACES_URL = "https://maps.googleapis.com/maps/api/place/"
    const val GOOGLE_PLACES_TYPE = "movie_theater"

//    object Filter {
//
//        const val FILTER_TYPE_MOVIE = "movie"
//        const val FILTER_TYPE_TV = "tv"
//        const val FILTER_GENRE = "with_genres"
//        const val FILTER_DATE_FROM = "release_date.gte"
//        const val FILTER_DATE_TO = "release_date.lte"
//
//        @StringDef(FILTER_TYPE_MOVIE, FILTER_TYPE_TV)
//        annotation class DataType
//
//    }

}