package com.iliarb.tmdbexplorer.data

/**
 * @author ilya on 3/11/17.
 */
object Preferences {

    const val KEY_SESSION_ID = "session_id"
    const val KEY_ID = "id"
    const val KEY_USERNAME = "username"
    const val KEY_NAME = "name"

}