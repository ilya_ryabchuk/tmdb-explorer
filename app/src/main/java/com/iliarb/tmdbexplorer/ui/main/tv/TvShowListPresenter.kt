package com.iliarb.tmdbexplorer.ui.main.tv

import com.iliarb.tmdbexplorer.data.model.tv_show.TvShowModel
import com.iliarb.tmdbexplorer.ui.base.BasePresenter
import io.reactivex.functions.Consumer
import timber.log.Timber
import javax.inject.Inject

/**
 * @author Ilya Ryabchuk on 06.11.16.
 */
class TvShowListPresenter @Inject constructor(model: TvShowModel, internal val adapter: TvShowAdapter)
    : BasePresenter<TvShowListView>() {

    private val tvShowModel: TvShowModel = model

    init {
        adapter.callback = { position ->
            mvpView?.showTvShowDetails(adapter.get(position))
        }
    }

    internal fun getTvShows() {
        tvShowModel.getTvShows("airing_today", 1).subscribe(Consumer {
            adapter.addAll(it)
        }, defaultErrorConsumer)
    }
}