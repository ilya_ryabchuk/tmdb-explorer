package com.iliarb.tmdbexplorer.ui.auth

import android.os.Bundle
import com.iliarb.tmdbexplorer.R
import com.iliarb.tmdbexplorer.ui.base.Presenter
import com.iliarb.tmdbexplorer.ui.base.inject.BaseInjectActivity
import com.iliarb.tmdbexplorer.util.Router
import com.iliarb.tmdbexplorer.util.extensions.initActionBar
import kotlinx.android.synthetic.main.activity_auth.*
import javax.inject.Inject

/**
 * @author Ilya Ryabchuk on 27.11.16.
 */
class AuthActivity : BaseInjectActivity(), AuthView {

    @Inject lateinit var presenter: AuthPresenter

    companion object {

        fun getRouteIntent(): Router.ActivityRouteBuilder<AuthActivity> {
            return Router.ActivityRouteBuilder(AuthActivity::class.java)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        uiComponent.inject(this)

        initActionBar(toolbar, addNavigationButton = true)

        btnLogin.setOnClickListener {
            presenter.validateWithLogin(authUsername.text.toString(), authPassword.text.toString())
        }

        presenter.attachView(this)
        presenter.createRequestToken()
    }

    override fun getPresenter(): Presenter<*>? = presenter

    override fun onLoginSuccess() {

    }

}