package com.iliarb.tmdbexplorer.ui.details.movie

import com.iliarb.tmdbexplorer.data.model.movie.MovieModel
import com.iliarb.tmdbexplorer.ui.base.BasePresenter
import io.reactivex.functions.Consumer
import javax.inject.Inject

/**
 * @author ilya on 3/19/17.
 */
class MovieDetailsPresenter @Inject constructor(model: MovieModel) : BasePresenter<MovieDetailsView>() {

    private val model: MovieModel = model

    internal fun getMovieDetails(id: Int) {
        model.getMovieDetails(id)
                .compose(applyProgress())
                .subscribe(Consumer { mvpView?.showMovieDetails(it) }, defaultErrorConsumer)
    }

}