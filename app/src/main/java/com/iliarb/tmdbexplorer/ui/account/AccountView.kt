package com.iliarb.tmdbexplorer.ui.account

import com.iliarb.tmdbexplorer.ui.base.MvpView

/**
 * @author Ilya Ryabchuk on 27.11.16.
 */
interface AccountView : MvpView {

    fun showUserDetails(name: String, username: String)

    fun onLoggedOut()

}