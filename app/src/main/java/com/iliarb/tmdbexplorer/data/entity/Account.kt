package com.iliarb.tmdbexplorer.data.entity

/**
 * @author Ilya Ryabchuk on 27.11.16.
 */
data class Account(val id: Int, val name: String, val username: String, val avatar: Avatar) {

    data class Avatar(val gravatar: Gravatar)

    data class Gravatar(val hash: String)

}