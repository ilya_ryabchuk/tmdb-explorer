package com.iliarb.tmdbexplorer.util

import java.text.SimpleDateFormat
import java.util.*

/**
 * @author Ilya Ryabchuk on 06.11.16.
 */
object DateUtils {

    private const val PARSE_FORMAT = "yyyy-MM-dd"
    private const val DISPLAY_FORMAT = "dd MMMM yyyy"

    fun getFormattedDate(date: String?) : String {
        if (date != null && date.isNotEmpty()) {
            val sdf = SimpleDateFormat(PARSE_FORMAT, Locale.getDefault())
            val display = SimpleDateFormat(DISPLAY_FORMAT, Locale.getDefault())
            return display.format(sdf.parse(date).time)
        }
        return ""
    }

    fun getAirDates(from: String?, to: String?) : String {
        if (from == null || from.isEmpty()) {
            return ""
        }

        val dateFrom = Calendar.getInstance()
        val dateTo = Calendar.getInstance()
        val sdf = SimpleDateFormat(PARSE_FORMAT, Locale.getDefault())

        dateFrom.time = sdf.parse(from)

        if (to == null || to.isEmpty()) {
            return dateFrom.get(Calendar.YEAR).toString()
        }

        dateTo.time = sdf.parse(to)

        val fromYear = dateFrom.get(Calendar.YEAR)
        val toYear = dateTo.get(Calendar.YEAR)

        if (fromYear != toYear) {
            return "$fromYear - $toYear"
        } else {
            return fromYear.toString()
        }
    }

    fun getYear(date: String?) : String {
        val calendar = Calendar.getInstance()
        if (date != null && date.isNotEmpty()) {
            val sdf = SimpleDateFormat(PARSE_FORMAT, Locale.getDefault())
            calendar.time = sdf.parse(date)
        }
        return calendar.get(Calendar.YEAR).toString()
    }

}