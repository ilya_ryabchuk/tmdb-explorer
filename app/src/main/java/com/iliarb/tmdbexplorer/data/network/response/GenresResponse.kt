package com.iliarb.tmdbexplorer.data.network.response

import com.iliarb.tmdbexplorer.data.entity.Genre

/**
 * @author Ilya Ryabchuk on 27.11.16.
 */
data class GenresResponse(val genres: MutableList<Genre>)