package com.iliarb.tmdbexplorer.data.entity

/**
 * @author Ilya Ryabchuk on 26.10.16.
 */
data class Backdrop(val filePath: String)