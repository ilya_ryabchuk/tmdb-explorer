package com.iliarb.tmdbexplorer.ui.base

/**
 * @author ilya on 3/11/17.
 */
interface Presenter<in V : MvpView> {

    fun attachView(mvpView: V)

    fun detachView()

}