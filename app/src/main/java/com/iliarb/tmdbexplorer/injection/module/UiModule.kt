package com.iliarb.tmdbexplorer.injection.module

import android.support.v4.app.FragmentManager
import com.iliarb.tmdbexplorer.injection.UserInterfaceProvider
import com.iliarb.tmdbexplorer.injection.scope.UiScope
import dagger.Module
import dagger.Provides

/**
 * @author Ilya Ryabchuk on 23.11.16.
 */
@Module
class UiModule(val provider: UserInterfaceProvider) {

    @Provides
    @UiScope
    fun provideSupportFragmentManager() : FragmentManager = provider.provideSupportFragmentManager()

}