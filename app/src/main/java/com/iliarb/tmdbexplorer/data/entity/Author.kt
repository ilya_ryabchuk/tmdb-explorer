package com.iliarb.tmdbexplorer.data.entity

/**
 * @author Ilya Ryabchuk on 06.11.16.
 */
data class Author(val id: Int, val name: String, val profilePath: String)