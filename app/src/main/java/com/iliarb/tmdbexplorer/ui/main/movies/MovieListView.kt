package com.iliarb.tmdbexplorer.ui.main.movies

import com.iliarb.tmdbexplorer.data.entity.Movie
import com.iliarb.tmdbexplorer.ui.base.MvpView

/**
 * @author Ilya Ryabchuk on 06.11.16.
 */
interface MovieListView : MvpView {

    fun showMovieDetails(movie: Movie)

}