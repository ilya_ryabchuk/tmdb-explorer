package com.iliarb.tmdbexplorer.data.entity

/**
 * @author Ilya Ryabchuk on 25.10.16.
 */
data class Keyword(val id: Int, val name: String)