package com.iliarb.tmdbexplorer.data.model.tv_show

import com.iliarb.tmdbexplorer.data.entity.TvShow
import com.iliarb.tmdbexplorer.data.entity.Video
import com.iliarb.tmdbexplorer.data.network.response.ListResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * @author Ilya Ryabchuk on 06.11.16.
 */
interface TvShowService {

    @GET("tv/{id}")
    fun getTvShowDetails(@Path("id") id: Int,
                         @Query("append_to_response") append: String) : Observable<TvShow>

    @GET("tv/{type}")
    fun getTvShows(@Path("type") type: String,
                   @Query("page") page: Int) : Single<ListResponse<TvShow>>

    @GET("tv/{id}/videos")
    fun getTvShowVideos(@Path("id") id: Int) : Observable<ListResponse<Video>>
}