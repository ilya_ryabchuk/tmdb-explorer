package com.iliarb.tmdbexplorer.ui.base

import android.support.annotation.StringRes

/**
 * @author ilya on 3/11/17.
 */
interface MvpView {

    fun showProgressDialog()

    fun hideProgressDialog()

    fun showMessage(message: String, errorCode: Int = -1)

    fun showMessage(@StringRes messageRes: Int, errorCode: Int = -1)

}