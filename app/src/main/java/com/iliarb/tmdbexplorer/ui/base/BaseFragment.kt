package com.iliarb.tmdbexplorer.ui.base

import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import com.iliarb.tmdbexplorer.ui.widget.dialog.MessageDialog

/**
 * @author ilya on 3/11/17.
 */
abstract class BaseFragment : Fragment(), MvpView {

    override fun showMessage(@StringRes messageRes: Int, errorCode: Int) {
        showMessage(getString(messageRes), errorCode)
    }

    override fun showMessage(message: String, errorCode: Int) {
        MessageDialog.newInstance(message, null).show(fragmentManager)
    }

    override fun showProgressDialog() {

    }

    override fun hideProgressDialog() {

    }
}