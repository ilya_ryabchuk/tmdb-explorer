package com.iliarb.tmdbexplorer.ui.main.tv

import android.support.v7.widget.util.SortedListAdapterCallback
import android.view.ViewGroup
import com.iliarb.tmdbexplorer.R
import com.iliarb.tmdbexplorer.data.entity.TvShow
import com.iliarb.tmdbexplorer.ui.base.recyclerview.RecyclerViewBaseAdapter
import com.iliarb.tmdbexplorer.util.extensions.inflate
import javax.inject.Inject

/**
 * @author Ilya Ryabchuk on 06.11.16.
 */
class TvShowAdapter @Inject constructor() : RecyclerViewBaseAdapter<TvShow, TvShowViewHolder>() {

    var callback: (Int) -> Unit = { _ -> }

    init {
        initList(TvShow::class.java, callback = object : SortedListAdapterCallback<TvShow>(this) {
            override fun compare(o1: TvShow?, o2: TvShow?): Int = 0

            override fun areItemsTheSame(item1: TvShow?, item2: TvShow?): Boolean {
                return item1?.id == item2?.id
            }

            override fun areContentsTheSame(oldItem: TvShow?, newItem: TvShow?): Boolean = false
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): TvShowViewHolder {
        return TvShowViewHolder(parent!!.inflate(R.layout.item_tv_show), callback)
    }
}