package com.iliarb.tmdbexplorer.ui.main.movies

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.iliarb.tmdbexplorer.R
import com.iliarb.tmdbexplorer.data.entity.Movie
import com.iliarb.tmdbexplorer.ui.base.Presenter
import com.iliarb.tmdbexplorer.ui.base.inject.BaseInjectFragment
import com.iliarb.tmdbexplorer.ui.details.movie.MovieDetailsActivity
import com.iliarb.tmdbexplorer.util.recyclerview.decoration.SpaceItemDecoration
import kotlinx.android.synthetic.main.fragment_movie_list.*
import kotlinx.android.synthetic.main.widget_tag_list.view.*
import javax.inject.Inject

/**
 * @author Ilya Ryabchuk on 22.10.16.
 */
class MovieListFragment : BaseInjectFragment(), MovieListView {

    @Inject lateinit var presenter: MovieListPresenter

    companion object {

        fun newInstance() = MovieListFragment()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        uiComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
            = inflater?.inflate(R.layout.fragment_movie_list, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tagList.tagContainer.setOnCheckedChangeListener { _, i -> presenter.onTagChanged(tagList.tags[i - 1]) }

        with(itemsList) {
            layoutManager = LinearLayoutManager(activity)
            adapter = presenter.adapter
            itemAnimator = DefaultItemAnimator()
            isNestedScrollingEnabled = false
            setHasFixedSize(true)

            val spacing = resources.getDimensionPixelSize(R.dimen.movie_list_spacing)
            addItemDecoration(SpaceItemDecoration(spacing, false, true))
        }

        presenter.type = tagList.getDefaultTag().code
        presenter.attachView(this)
        presenter.getMovies(1)
    }

    override fun getPresenter(): Presenter<*>? = presenter

    override fun showMovieDetails(movie: Movie) {
        MovieDetailsActivity.route(movie.id, movie.title, movie.backdropPath).startFrom(activity)
    }
}