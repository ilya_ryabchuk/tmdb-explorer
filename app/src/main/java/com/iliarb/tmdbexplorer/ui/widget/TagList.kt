package com.iliarb.tmdbexplorer.ui.widget

import android.content.Context
import android.content.res.ColorStateList
import android.support.v4.content.ContextCompat
import android.support.v7.widget.AppCompatRadioButton
import android.util.AttributeSet
import android.widget.RadioGroup
import android.widget.ScrollView
import com.iliarb.tmdbexplorer.R
import com.iliarb.tmdbexplorer.data.entity.Tag
import kotlinx.android.synthetic.main.widget_tag_list.view.*

/**
 * @author ilya on 3/11/17.
 */
class TagList : ScrollView {

    var tags: MutableList<Tag> = mutableListOf()

    val paddingVertical: Int
    val paddingHorizontal: Int
    val itemSpacing: Int
    val itemTextColor: ColorStateList
    val itemBackground: Int

    constructor(context: Context?) : this(context, null, 0)

    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context?,
                attrs: AttributeSet?,
                defStyleAttr: Int) : super(context, attrs, defStyleAttr) {

        inflate(context, R.layout.widget_tag_list, this)

        paddingHorizontal = resources.getDimensionPixelSize(R.dimen.activity_horizontal_half_margin)
        paddingVertical = resources.getDimensionPixelSize(R.dimen.activity_horizontal_margin)
        itemSpacing = resources.getDimensionPixelSize(R.dimen.tags_spacing)

        itemTextColor = ContextCompat.getColorStateList(context, R.color.checkbox_tag)
        itemBackground = R.drawable.bg_item_tag_selector

        if (context != null) {
            context.resources.getStringArray(R.array.movie_tags).forEach {
                tags.add(Tag(it, false, createTagCode(it)))
            }
            showList()
        }
    }

    fun getDefaultTag() : Tag = tags[0]

    private fun showList() {
        for ((index, value) in tags.withIndex()) {

            val button = AppCompatRadioButton(context)
            val params = RadioGroup.LayoutParams(
                    RadioGroup.LayoutParams.WRAP_CONTENT,
                    RadioGroup.LayoutParams.WRAP_CONTENT)

            if (index == tags.size - 1) {
                params.setMargins(itemSpacing, 0, itemSpacing, 0)
            } else {
                params.setMargins(itemSpacing, 0, 0, 0)
            }

            with(button) {
                setBackgroundResource(itemBackground)
                setButtonDrawable(android.R.color.transparent)
                setPadding(paddingVertical, paddingHorizontal, paddingVertical, paddingHorizontal)
                setTextColor(itemTextColor)

                layoutParams = params
                text = value.title
            }

            tagContainer.addView(button)

            (tagContainer.getChildAt(0) as AppCompatRadioButton).isChecked = true
        }
    }

    private fun createTagCode(tag: String): String = tag.toLowerCase().replace(" ", "_")
}