package com.iliarb.tmdbexplorer.data.network.response

/**
 * @author Ilya Ryabchuk on 23.10.16.
 */
data class ListResponse<T>(val page: Int, val results: MutableList<T>)