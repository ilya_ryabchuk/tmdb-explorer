package com.iliarb.tmdbexplorer.data.entity

/**
 * @author Ilya Ryabchuk on 20.11.16.
 */
data class Person(
        val id: Int,
        val name: String,
        val profilePath: String,
        val biography: String,
        val birthday: String,
        val deathday: String,
        val homepage: String,
        val placeOfBirth: String,
        val images: PersonImages)