package com.iliarb.tmdbexplorer.util.recyclerview.decoration

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.State
import android.view.View

/**
 * @author Ilya Ryabchuk on 26.10.16.
 */
class DetailsHorizontalItemDecoration(
        val itemSpacing: Int,
        val edgeSpacing: Int) : RecyclerView.ItemDecoration() {

    constructor(spacing: Int) : this(spacing, spacing)

    override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: State?) {
        if (parent?.getChildAdapterPosition(view) == 0) {
            outRect?.left = edgeSpacing
            outRect?.right = itemSpacing

        } else if (parent?.getChildAdapterPosition(view) == parent?.adapter?.itemCount?.minus(1)) {
            outRect?.right = edgeSpacing

        } else {
            outRect?.right = itemSpacing
        }
    }
}