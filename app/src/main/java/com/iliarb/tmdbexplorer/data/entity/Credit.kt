package com.iliarb.tmdbexplorer.data.entity

/**
 * @author Ilya Ryabchuk on 21.11.16.
 */
data class Credit(val id: Int, val title: String, val posterPath: String, val character: String)