package com.iliarb.tmdbexplorer.ui.details.common.view_holder

import android.view.View
import com.iliarb.tmdbexplorer.data.entity.Cast
import com.iliarb.tmdbexplorer.ui.base.recyclerview.BaseViewHolder

/**
 * @author Ilya Ryabchuk on 30.03.17.
 */
class CreditViewHolder(view: View) : BaseViewHolder<Cast>(view) {

    init {

    }

    override fun prepareForReuse() {

    }

    override fun bind(item: Cast, position: Int) {

    }
}