package com.iliarb.tmdbexplorer.data.entity

/**
 * @author Ilya Ryabchuk on 06.11.16.
 */
data class Season(
        val airDate: String,
        val episodeCount: Int,
        val id: Int,
        val posterPath: String,
        val seasonNumber: Int)