package com.iliarb.tmdbexplorer.injection.scope

import javax.inject.Scope

/**
 * @author Ilya Ryabchuk on 23.11.16.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class UiScope