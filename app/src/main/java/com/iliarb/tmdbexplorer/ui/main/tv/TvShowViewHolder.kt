package com.iliarb.tmdbexplorer.ui.main.tv

import android.view.View
import com.bumptech.glide.Glide
import com.iliarb.tmdbexplorer.R
import com.iliarb.tmdbexplorer.data.entity.TvShow
import com.iliarb.tmdbexplorer.ui.base.recyclerview.BaseViewHolder
import com.iliarb.tmdbexplorer.util.DateUtils
import com.iliarb.tmdbexplorer.util.extensions.loadImage
import kotlinx.android.synthetic.main.item_tv_show.view.*

/**
 * @author Ilya Ryabchuk on 06.11.16.
 */
class TvShowViewHolder(view: View, private val callback: (Int) -> Unit) : BaseViewHolder<TvShow>(view) {

    init {
        itemView.setOnClickListener { callback.invoke(adapterPosition) }
    }

    override fun bind(item: TvShow, position: Int) {
        itemView.itemImage.loadImage(item.backdropPath)

        val date = DateUtils.getAirDates(item.firstAirDate, item.lastAirDate)
        itemView.itemTitle.text = itemView.context.getString(R.string.item_tv_show_title, item.name, date)
    }

    override fun prepareForReuse() = Glide.clear(itemView.itemImage)

}