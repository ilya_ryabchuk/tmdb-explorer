package com.iliarb.tmdbexplorer.data.network.response

import com.iliarb.tmdbexplorer.data.entity.Video

/**
 * @author Ilya Ryabchuk on 01.11.16.
 */
data class VideoResponse(val results: MutableList<Video>)