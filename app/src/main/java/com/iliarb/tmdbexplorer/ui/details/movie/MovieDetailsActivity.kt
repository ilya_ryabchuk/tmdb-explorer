package com.iliarb.tmdbexplorer.ui.details.movie

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.iliarb.tmdbexplorer.R
import com.iliarb.tmdbexplorer.data.entity.Movie
import com.iliarb.tmdbexplorer.ui.base.Presenter
import com.iliarb.tmdbexplorer.ui.base.inject.BaseInjectActivity
import com.iliarb.tmdbexplorer.ui.details.common.adapter.ImagesAdapter
import com.iliarb.tmdbexplorer.util.Router
import com.iliarb.tmdbexplorer.util.extensions.initActionBar
import com.iliarb.tmdbexplorer.util.extensions.loadImage
import com.iliarb.tmdbexplorer.util.recyclerview.decoration.DetailsHorizontalItemDecoration
import kotlinx.android.synthetic.main.activity_movie_details.*
import java.text.NumberFormat
import java.util.*
import javax.inject.Inject

/**
 * @author ilya on 3/19/17.
 */
class MovieDetailsActivity : BaseInjectActivity(), MovieDetailsView {

    @Inject lateinit var presenter: MovieDetailsPresenter

    companion object {

        private const val EXTRA_ID = "id"
        private const val EXTRA_TITLE = "title"
        private const val EXTRA_IMAGE = "image"

        fun route(id: Int, title: String = "", image: String = ""): Router.ActivityRouteBuilder<MovieDetailsActivity> {
            return Router.ActivityRouteBuilder(MovieDetailsActivity::class.java)
                    .putExtra(EXTRA_ID, id)
                    .putExtra(EXTRA_TITLE, title)
                    .putExtra(EXTRA_IMAGE, image)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)
        uiComponent.inject(this)

        initActionBar(toolbar, title = intent.getStringExtra(EXTRA_TITLE), addNavigationButton = true)

        val image = intent.getStringExtra(EXTRA_IMAGE)
        if (!image.isNullOrBlank()) {
            moviePoster.loadImage(image)
        }

        presenter.attachView(this)
        presenter.getMovieDetails(intent.getIntExtra(EXTRA_ID, -1))
    }

    override fun showMovieDetails(movie: Movie) {

        supportActionBar?.title = movie.title

        moviePoster.loadImage(movie.backdropPath)
        movieOverview.setComponentText(movie.overview)

        if (movie.budget != 0L) {
            movieBudget.setComponentText(NumberFormat.getInstance(Locale.FRANCE).format(movie.budget))
        }

        movieImages.setupComponentList(
                adapter = ImagesAdapter(movie.images.backdrops, { _ -> }),
                layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false),
                hasFixesSize = true,
                isNestedScrollingEnabled = false,
                itemDecoration = DetailsHorizontalItemDecoration(resources.getDimensionPixelSize(R.dimen.activity_horizontal_margin)))
    }

    override fun getPresenter(): Presenter<*>? = presenter
}