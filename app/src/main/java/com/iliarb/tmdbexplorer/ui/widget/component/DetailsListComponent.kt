package com.iliarb.tmdbexplorer.ui.widget.component

import android.content.Context
import android.support.annotation.StringRes
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.widget.LinearLayout
import com.iliarb.tmdbexplorer.R
import kotlinx.android.synthetic.main.stub_component_list.view.*

/**
 * @author ilya on 3/19/17.
 */

class DetailsListComponent @JvmOverloads constructor(context: Context,
                                                     attrs: AttributeSet? = null,
                                                     defStyleAttr: Int = 0)
    : LinearLayout(context, attrs, defStyleAttr) {

    init {
        orientation = VERTICAL

        inflate(context, R.layout.stub_component_list, this)

        if (attrs != null) {
            val attributes = context.obtainStyledAttributes(attrs, R.styleable.DetailsListComponent)

            val title = attributes.getString(R.styleable.DetailsListComponent_listComponentTitle)
            if (title != null) {
                componentTitle.text = title
            }

            attributes.recycle()
        }
    }

    fun setTitle(title: String) {
        componentTitle.text = title
    }

    fun setTitle(@StringRes titleRes: Int) {
        componentTitle.text = context.getString(titleRes)
    }

    fun setLayoutManager(manager: RecyclerView.LayoutManager) {
        componentList.layoutManager = manager
    }

    fun setAdapter(adapter: RecyclerView.Adapter<*>) {
        componentList.adapter = adapter
    }

    fun setupComponentList(
            adapter: RecyclerView.Adapter<*>? = null,
            layoutManager: RecyclerView.LayoutManager? = null,
            hasFixesSize: Boolean = false,
            isNestedScrollingEnabled: Boolean = true,
            itemDecoration: RecyclerView.ItemDecoration? = null) {

        if (adapter != null) {
            componentList.adapter = adapter
        }

        if (layoutManager != null) {
            componentList.layoutManager = layoutManager
        }

        if (itemDecoration != null) {
            componentList.addItemDecoration(itemDecoration)
        }

        componentList.isNestedScrollingEnabled = isNestedScrollingEnabled
        componentList.setHasFixedSize(hasFixesSize)
    }
}
