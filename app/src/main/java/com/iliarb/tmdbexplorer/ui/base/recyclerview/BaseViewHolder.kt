package com.iliarb.tmdbexplorer.ui.base.recyclerview

import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * @author ilya on 3/11/17.
 */
abstract class BaseViewHolder<in M>(itemView: View) : RecyclerView.ViewHolder(itemView) {

    /**
     * Called when view is being recycled to clear resources
     */
    abstract fun prepareForReuse()

    /**
     * Fills view holder with model data
     */
    abstract fun bind(item: M, position: Int)

}