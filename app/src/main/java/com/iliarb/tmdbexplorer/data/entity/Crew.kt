package com.iliarb.tmdbexplorer.data.entity

/**
 * @author Ilya Ryabchuk on 20.11.16.
 */
data class Crew(val department: String, val name: String, val profilePath: String)