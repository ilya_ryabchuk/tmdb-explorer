package com.iliarb.tmdbexplorer

import android.app.Application
import android.content.Context
import com.frogermcs.androiddevmetrics.AndroidDevMetrics
import com.iliarb.tmdbexplorer.injection.component.ApplicationComponent
import com.iliarb.tmdbexplorer.injection.component.DaggerApplicationComponent
import com.iliarb.tmdbexplorer.injection.module.ApplicationModule
import com.iliarb.tmdbexplorer.injection.module.NetworkModule
import com.iliarb.tmdbexplorer.injection.module.UtilsModule
import com.squareup.leakcanary.LeakCanary
import timber.log.Timber
import timber.log.Timber.DebugTree

/**
 * @author Ilya Ryabchuk on 20.10.16.
 */
class App : Application() {

    private var _appComponent: ApplicationComponent? = null

    companion object {

        operator fun get(context: Context): App = context.applicationContext as App

    }

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            AndroidDevMetrics.initWith(this)
            LeakCanary.install(this)
            Timber.plant(DebugTree())
        }
    }

    val appComponent: ApplicationComponent
        get() {
            if (_appComponent == null) {
                _appComponent = DaggerApplicationComponent.builder()
                        .applicationModule(ApplicationModule(this))
                        .networkModule(NetworkModule())
                        .utilsModule(UtilsModule())
                        .build()
            }
            return _appComponent!!
        }
}