package com.iliarb.tmdbexplorer.data.model.auth

import com.iliarb.tmdbexplorer.data.Preferences
import com.iliarb.tmdbexplorer.data.entity.Account
import com.iliarb.tmdbexplorer.data.model.BaseModel
import com.iliarb.tmdbexplorer.data.network.response.RequestTokenResponse
import com.iliarb.tmdbexplorer.util.extensions.applySchedulers
import com.iliarb.tmdbexplorer.util.rx.RxSharedPreferences
import com.iliarb.tmdbexplorer.util.rx.RxSharedPreferences.PreferenceType
import io.reactivex.Single
import javax.inject.Inject

/**
 * @author Ilya Ryabchuk on 27.11.16.
 */
class AuthModel @Inject constructor(auth: AuthService,
                                    account: AccountService,
                                    prefs: RxSharedPreferences) : BaseModel() {

    private val authService: AuthService = auth
    private val accountService: AccountService = account
    private val preferences: RxSharedPreferences = prefs

    fun createRequestToken(): Single<RequestTokenResponse> {
        return authService.createRequestToken()
                .applySchedulers()
                .compose(transformException())
    }

    fun authorize(token: String, username: String, password: String): Single<Account> {
        return authService.validateWithLogin(username, password, token)
                .flatMap { (_, requestToken) -> authService.authorize(requestToken) }
                .flatMap {
                    preferences.putValue(Preferences.KEY_SESSION_ID, PreferenceType.String(it.sessionId))

                    accountService.getAccountDetails(it.sessionId)
                }
                .map {
                    preferences.putValue(Preferences.KEY_ID, PreferenceType.Int(it.id))
                    preferences.putValue(Preferences.KEY_NAME, PreferenceType.String(it.name))
                    preferences.putValue(Preferences.KEY_USERNAME, PreferenceType.String(it.username))

                    return@map it
                }
                .applySchedulers()
                .compose(transformException())
    }

}