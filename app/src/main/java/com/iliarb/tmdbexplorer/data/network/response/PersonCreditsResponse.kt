package com.iliarb.tmdbexplorer.data.network.response

import com.iliarb.tmdbexplorer.data.entity.Credit

/**
 * @author Ilya Ryabchuk on 21.11.16.
 */
data class PersonCreditsResponse(val cast: MutableList<Credit>)