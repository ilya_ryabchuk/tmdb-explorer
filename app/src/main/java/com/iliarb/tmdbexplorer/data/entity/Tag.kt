package com.iliarb.tmdbexplorer.data.entity

/**
 * @author Ilya Ryabchuk on 23.10.16.
 */
data class Tag(val title: String, val checked: Boolean, val code: String)