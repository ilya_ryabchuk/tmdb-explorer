package com.iliarb.tmdbexplorer.util.rx

import android.content.Context
import android.content.SharedPreferences
import com.iliarb.tmdbexplorer.injection.ApplicationContext
import io.reactivex.Completable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/**
 * @author Ilya Ryabchuk on 27.11.16.
 */
class RxSharedPreferences constructor(@ApplicationContext context: Context) {

    val prefsName: String = "${context.packageName}_shared_prefs"
    val preferences: SharedPreferences

    init {
        preferences = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE)
    }

    fun putValue(key: String,
                 value: PreferenceType,
                 onComplete: Action = Action {},
                 onError: Consumer<in Throwable> = Consumer { Timber.e(it) }) {
        putValue(key, value).subscribe(onComplete, onError)
    }

    fun putValue(key: String, value: PreferenceType): Completable {
        return Completable.create {
            val editor = preferences.edit()

            when (value) {
                is PreferenceType.String -> editor.putString(key, value.value)
                is PreferenceType.Int -> editor.putInt(key, value.value)
            }

            editor.apply()

            it.onComplete()

        }.subscribeOn(Schedulers.computation())
    }

    fun getString(key: String, fallback: String = ""): String = preferences.getString(key, fallback)

    fun getInt(key: String, fallback: Int = 0): Int = preferences.getInt(key, fallback)

    fun has(key: String, fallback: Boolean = false): Boolean = preferences.getBoolean(key, fallback)

    fun removeValue(key: String) = preferences.edit().remove(key).apply()

    fun clear() = preferences.edit().clear().apply()

    sealed class PreferenceType {

        class Int(val value: kotlin.Int) : PreferenceType()
        class String(val value: kotlin.String) : PreferenceType()

    }

}