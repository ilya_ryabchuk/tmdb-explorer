package com.iliarb.tmdbexplorer.ui.base.inject

import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import com.iliarb.tmdbexplorer.App
import com.iliarb.tmdbexplorer.injection.UserInterfaceProvider
import com.iliarb.tmdbexplorer.injection.component.UiComponent
import com.iliarb.tmdbexplorer.injection.module.UiModule
import com.iliarb.tmdbexplorer.ui.base.BaseActivity
import com.iliarb.tmdbexplorer.ui.base.Presenter
import com.iliarb.tmdbexplorer.util.extensions.addToOnDestroy
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * @author ilya on 3/11/17.
 */
abstract class BaseInjectActivity : BaseActivity(), UserInterfaceProvider {

    protected val onDestroyDisposable: CompositeDisposable = CompositeDisposable()

    val uiComponent: UiComponent
        get() {
            if (_component == null) {
                _component = App[this].appComponent.uiComponentBuilder()
                        .uiModule(UiModule(this))
                        .build()
            }
            return _component!!
        }

    private var _component: UiComponent? = null

    override fun onDestroy() {
        if (!onDestroyDisposable.isDisposed) {
            onDestroyDisposable.dispose()
        }

        getPresenter()?.detachView()
        super.onDestroy()
    }

    override fun provideSupportFragmentManager(): FragmentManager = supportFragmentManager

    override fun provideActivity(): AppCompatActivity = this

    protected inline fun injectAsync(crossinline callback: (UiComponent) -> Unit,
                                     crossinline onComplete: () -> Unit) {

        Completable.create { callback(uiComponent); it.onComplete() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { onComplete.invoke() }
                .addToOnDestroy(onDestroyDisposable)
    }

    /**
     * Returns presenter if exists
     * to detach from view on destroy
     */
    protected abstract fun getPresenter(): Presenter<*>?
}