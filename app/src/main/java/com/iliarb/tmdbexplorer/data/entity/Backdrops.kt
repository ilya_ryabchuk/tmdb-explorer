package com.iliarb.tmdbexplorer.data.entity

import java.util.*

/**
 * @author Ilya Ryabchuk on 26.10.16.
 */
data class Backdrops(val backdrops: MutableList<Backdrop>) {

    fun toImageList() : ArrayList<String> = backdrops.map { it.filePath }.toList() as ArrayList<String>

}