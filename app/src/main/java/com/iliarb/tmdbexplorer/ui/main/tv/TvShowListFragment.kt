package com.iliarb.tmdbexplorer.ui.main.tv

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.iliarb.tmdbexplorer.R
import com.iliarb.tmdbexplorer.data.entity.TvShow
import com.iliarb.tmdbexplorer.ui.base.Presenter
import com.iliarb.tmdbexplorer.ui.base.inject.BaseInjectFragment
import com.iliarb.tmdbexplorer.util.recyclerview.decoration.GridSpacingItemDecoration
import kotlinx.android.synthetic.main.fragment_tv_list.*
import javax.inject.Inject

/**
 * @author Ilya Ryabchuk on 22.10.16.
 */
class TvShowListFragment : BaseInjectFragment(), TvShowListView {

    @Inject lateinit var presenter: TvShowListPresenter

    companion object {

        fun newInstance() = TvShowListFragment()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        uiComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater?.inflate(R.layout.fragment_tv_list, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(itemsList) {
            layoutManager = GridLayoutManager(activity, 2)
            adapter = presenter.adapter
            setHasFixedSize(true)

            val spacing = resources.getDimensionPixelSize(R.dimen.tv_show_spacing)
            addItemDecoration(GridSpacingItemDecoration(2, spacing, false))
        }

        presenter.attachView(this)
        presenter.getTvShows()
    }

    override fun showTvShowDetails(tvShow: TvShow) {

    }

    override fun getPresenter(): Presenter<*>? = presenter
}