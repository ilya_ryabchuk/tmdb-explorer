package com.iliarb.tmdbexplorer.data.model.auth

import com.iliarb.tmdbexplorer.data.entity.Account
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author Ilya Ryabchuk on 27.11.16.
 */
interface AccountService {

    @GET("account")
    fun getAccountDetails(@Query("session_id") sessionId: String) : Single<Account>

}