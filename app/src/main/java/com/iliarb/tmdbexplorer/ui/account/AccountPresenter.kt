package com.iliarb.tmdbexplorer.ui.account

import com.iliarb.tmdbexplorer.data.Preferences
import com.iliarb.tmdbexplorer.ui.base.BasePresenter
import com.iliarb.tmdbexplorer.util.rx.RxSharedPreferences
import javax.inject.Inject

/**
 * @author Ilya Ryabchuk on 27.11.16.
 */
class AccountPresenter @Inject constructor(val preferences: RxSharedPreferences) : BasePresenter<AccountView>() {

    fun getUserDetails() {
        val username = preferences.getString(Preferences.KEY_USERNAME)
        val name = preferences.getString(Preferences.KEY_NAME)

        mvpView?.showUserDetails(name, username)
    }

    fun logout() {
        preferences.clear()
        mvpView?.onLoggedOut()
    }

}