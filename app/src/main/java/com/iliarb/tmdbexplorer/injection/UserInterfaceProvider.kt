package com.iliarb.tmdbexplorer.injection

import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity

/**
 * @author Ilya Ryabchuk on 23.11.16.
 */
interface UserInterfaceProvider {

    fun provideSupportFragmentManager() : FragmentManager

    fun provideActivity() : AppCompatActivity

}