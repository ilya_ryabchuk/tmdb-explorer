package com.iliarb.tmdbexplorer.data.model.movie

import com.iliarb.tmdbexplorer.data.entity.Movie
import com.iliarb.tmdbexplorer.data.model.BaseModel
import com.iliarb.tmdbexplorer.data.network.ApiConfig
import com.iliarb.tmdbexplorer.data.network.response.ListResponse
import com.iliarb.tmdbexplorer.util.extensions.applySchedulers
import io.reactivex.Single
import javax.inject.Inject

/**
 * @author Ilya Ryabchuk on 23.10.16.
 */
class MovieModel @Inject constructor(movieService: MovieService) : BaseModel() {

    private val service: MovieService = movieService

    fun getMovies(type: String, page: Int): Single<MutableList<Movie>> {
        return service.getMovies(type, page)
                .applySchedulers()
                .compose(transformException())
                .map(ListResponse<Movie>::results)
    }

    fun getMovieDetails(id: Int): Single<Movie> {
        return service.getMovieDetails(id, ApiConfig.APPEND_MOVIE_DETAILS)
                .applySchedulers()
                .compose(transformException())
    }

}