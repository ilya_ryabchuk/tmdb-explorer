package com.iliarb.tmdbexplorer.data.entity

import java.util.*

/**
 * @author Ilya Ryabchuk on 20.11.16.
 */
data class PersonImages(val profiles: MutableList<Backdrop>) {

    fun toImageList() : ArrayList<String> = profiles.map { it.filePath }.toList() as ArrayList<String>

}