package com.iliarb.tmdbexplorer;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleInstrumentedTest {

    public void useAppContext() {
        // Context of the app under test
    }
}
