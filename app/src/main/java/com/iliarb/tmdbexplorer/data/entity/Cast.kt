package com.iliarb.tmdbexplorer.data.entity

/**
 * @author Ilya Ryabchuk on 20.11.16.
 */
data class Cast(val id: Int, val character: String, val profilePath: String, val name: String)