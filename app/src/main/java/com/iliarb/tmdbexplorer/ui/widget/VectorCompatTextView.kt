package com.iliarb.tmdbexplorer.ui.widget

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.support.graphics.drawable.VectorDrawableCompat
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import com.iliarb.tmdbexplorer.R

/**
 * @author Ilya Ryabchuk on 17.03.17.
 */
class VectorCompatTextView : AppCompatTextView {

    constructor(context: Context) : super(context) {
        initialize(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialize(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize(context, attrs)
    }

    private fun initialize(context: Context, attrs: AttributeSet?) {
        if (attrs != null) {
            val attributes = context.obtainStyledAttributes(attrs, R.styleable.VectorCompatTextView)

            var drawableLeft: Drawable? = null
            var drawableRight: Drawable? = null
            var drawableTop: Drawable? = null
            var drawableBottom: Drawable? = null

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                drawableLeft = attributes.getDrawable(R.styleable.VectorCompatTextView_drawableLeftCompat)
                drawableRight = attributes.getDrawable(R.styleable.VectorCompatTextView_drawableRightCompat)
                drawableTop = attributes.getDrawable(R.styleable.VectorCompatTextView_drawableTopCompat)
                drawableBottom = attributes.getDrawable(R.styleable.VectorCompatTextView_drawableBottomCompat)

            } else {
                val drawableLeftId = attributes.getResourceId(R.styleable.VectorCompatTextView_drawableLeftCompat, -1)
                if (drawableLeftId != -1) {
                    drawableLeft = VectorDrawableCompat.create(context.resources, drawableLeftId, null)
                }

                val drawableTopId = attributes.getResourceId(R.styleable.VectorCompatTextView_drawableTopCompat, -1)
                if (drawableTopId != -1) {
                    drawableTop = VectorDrawableCompat.create(context.resources, drawableTopId, null)
                }

                val drawableRightId = attributes.getResourceId(R.styleable.VectorCompatTextView_drawableRightCompat, -1)
                if (drawableRightId != -1) {
                    drawableRight = VectorDrawableCompat.create(context.resources, drawableRightId, null)
                }

                val drawableBottomId = attributes.getResourceId(R.styleable.VectorCompatTextView_drawableBottomCompat, -1)
                if (drawableBottomId != -1) {
                    drawableBottom = VectorDrawableCompat.create(context.resources, drawableBottomId, null)
                }
            }

            setCompoundDrawablesWithIntrinsicBounds(drawableLeft, drawableTop, drawableRight, drawableBottom)
            attributes.recycle()
        }
    }
}