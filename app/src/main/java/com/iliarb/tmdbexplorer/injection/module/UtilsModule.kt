package com.iliarb.tmdbexplorer.injection.module

import android.content.Context
import com.iliarb.tmdbexplorer.injection.ApplicationContext
import com.iliarb.tmdbexplorer.util.rx.RxSharedPreferences
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author Ilya Ryabchuk on 27.11.16.
 */
@Module
class UtilsModule {

    @Provides
    @Singleton
    fun provideSharedPreferences(@ApplicationContext context: Context) : RxSharedPreferences {
        return RxSharedPreferences(context)
    }

}