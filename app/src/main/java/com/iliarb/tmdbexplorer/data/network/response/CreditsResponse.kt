package com.iliarb.tmdbexplorer.data.network.response

import com.iliarb.tmdbexplorer.data.entity.Cast
import com.iliarb.tmdbexplorer.data.entity.Crew

/**
 * @author Ilya Ryabchuk on 20.11.16.
 */
data class CreditsResponse(val crew: MutableList<Crew>, val cast: MutableList<Cast>)