package com.iliarb.tmdbexplorer.util.extensions

import android.support.annotation.DrawableRes
import android.support.annotation.LayoutRes
import android.support.annotation.MenuRes
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * @author Ilya Ryabchuk on 21.11.16.
 */

fun AppCompatActivity.initActionBar(toolbar: Toolbar,
                                    title: String? = null,
                                    addNavigationButton: Boolean = false,
                                    navigationClickListener: View.OnClickListener? = null,
                                    @DrawableRes navigationIcon: Int = 0,
                                    @MenuRes menu: Int = 0,
                                    menuClickListener: Toolbar.OnMenuItemClickListener? = null) {

    if (title != null) {
        toolbar.title = title
    }

    if (navigationClickListener != null) {
        toolbar.setNavigationOnClickListener(navigationClickListener)
    }

    if (navigationIcon != 0) {
        toolbar.setNavigationIcon(navigationIcon)
    }

    if (menu != 0) {
        toolbar.inflateMenu(menu)

        if (menuClickListener != null) {
            toolbar.setOnMenuItemClickListener(menuClickListener)
        }
    }

    setSupportActionBar(toolbar)

    if (supportActionBar != null) {
        supportActionBar!!.setDisplayHomeAsUpEnabled(addNavigationButton)
    }
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

infix fun View.show(show: Boolean) {
    if (show) {
        visibility = View.VISIBLE
    } else {
        visibility = View.GONE
    }
}