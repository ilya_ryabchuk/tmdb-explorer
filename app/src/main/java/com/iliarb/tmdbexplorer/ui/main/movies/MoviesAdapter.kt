package com.iliarb.tmdbexplorer.ui.main.movies

import android.support.v7.widget.util.SortedListAdapterCallback
import android.view.ViewGroup
import com.iliarb.tmdbexplorer.R
import com.iliarb.tmdbexplorer.data.entity.Movie
import com.iliarb.tmdbexplorer.ui.base.recyclerview.RecyclerViewBaseAdapter
import com.iliarb.tmdbexplorer.util.extensions.inflate
import javax.inject.Inject

/**
 * @author Ilya Ryabchuk on 23.10.16.
 */
class MoviesAdapter @Inject constructor() : RecyclerViewBaseAdapter<Movie, MovieViewHolder>() {

    var callback: (Int) -> Unit = { _ -> }

    init {
        initList(Movie::class.java, callback = object : SortedListAdapterCallback<Movie>(this) {
            
            override fun compare(o1: Movie?, o2: Movie?): Int = 0

            override fun areItemsTheSame(item1: Movie?, item2: Movie?): Boolean {
                return item1?.id == item2?.id
            }

            override fun areContentsTheSame(oldItem: Movie?, newItem: Movie?): Boolean {
                return oldItem?.title == newItem?.title
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MovieViewHolder {
        return MovieViewHolder(parent!!.inflate(R.layout.item_movie), callback)
    }
}