package com.iliarb.tmdbexplorer.data.entity

/**
 * @author Ilya Ryabchuk on 27.11.16.
 */
data class Session(val success: Boolean, val sessionId: String)