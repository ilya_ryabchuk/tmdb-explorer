package com.iliarb.tmdbexplorer.ui.main.tv

import com.iliarb.tmdbexplorer.data.entity.TvShow
import com.iliarb.tmdbexplorer.ui.base.MvpView

/**
 * @author Ilya Ryabchuk on 06.11.16.
 */
interface TvShowListView : MvpView {

    fun showTvShowDetails(tvShow: TvShow)

}