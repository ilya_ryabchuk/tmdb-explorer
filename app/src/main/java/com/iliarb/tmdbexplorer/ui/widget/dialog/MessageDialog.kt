package com.iliarb.tmdbexplorer.ui.widget.dialog

import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.iliarb.tmdbexplorer.R
import com.iliarb.tmdbexplorer.util.extensions.show
import kotlinx.android.synthetic.main.dialog_message.*

/**
 * @author ilya on 3/14/17.
 */
class MessageDialog : BottomSheetDialogFragment() {

    companion object {

        private const val ARG_MESSAGE = "message"
        private const val ARG_TITLE = "title"

        fun newInstance(message: String, title: String?): MessageDialog {
            val dialog = MessageDialog()
            val args = Bundle()

            args.putString(ARG_MESSAGE, message)
            args.putString(ARG_TITLE, title)

            dialog.arguments = args

            return dialog
        }
    }

    override fun onCreateView(inflater: LayoutInflater?,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater?.inflate(R.layout.dialog_message, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            val title = arguments.getString(ARG_TITLE)
            if (title != null) {
                dialogTitle.text = title
            } else {
                dialogTitle show false
            }

            val message = arguments.getString(ARG_MESSAGE)
            if (message != null) {
                dialogMessage.text = message
            }
        }
    }

    fun show(fragmentManager: FragmentManager) {
        show(fragmentManager, MessageDialog::class.java.simpleName)
    }

}