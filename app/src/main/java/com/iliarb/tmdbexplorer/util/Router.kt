package com.iliarb.tmdbexplorer.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.support.v4.app.Fragment

/**
 * @author Ilya Ryabchuk on 22.10.16.
 */

object Router {

    class ActivityRouteBuilder<T : Activity> {

        private val intent: Intent
        private var activityClass: Class<T>? = null

        constructor(intent: Intent) {
            this.intent = intent
        }

        constructor(clazz: Class<T>) {
            activityClass = clazz
            intent = Intent()
        }

        fun setFlags(flags: Int): ActivityRouteBuilder<T> {
            intent.flags = flags
            return this
        }

        fun addFlags(flags: Int): ActivityRouteBuilder<T> {
            intent.addFlags(flags)
            return this
        }

        fun clearBackStack(): ActivityRouteBuilder<T> {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK.xor(Intent.FLAG_ACTIVITY_NEW_TASK))
            return this
        }

        fun activityClass(clazz: Class<T>): ActivityRouteBuilder<T> {
            activityClass = clazz
            return this
        }

        fun newTaskAndSingleTop(): ActivityRouteBuilder<T> {
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    .xor(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .xor(Intent.FLAG_ACTIVITY_NEW_TASK))
            return this
        }

        fun putExtra(key: String, value: Any): ActivityRouteBuilder<T> {
            when (value) {
                is Int -> intent.putExtra(key, value)
                is String -> intent.putExtra(key, value)
                is Boolean -> intent.putExtra(key, value)
                is Parcelable -> intent.putExtra(key, value)
            }
            return this
        }

        fun startFrom(context: Context) {
            intent.setClass(context, activityClass)
            context.startActivity(intent)
        }

        fun startFrom(activity: Activity) {
            intent.setClass(activity, activityClass)
            activity.startActivity(intent)
        }

        fun startFrom(fragment: Fragment) {
            intent.setClass(fragment.activity, activityClass)
            fragment.startActivity(intent)
        }

    }
}