package com.iliarb.tmdbexplorer.injection.module

import android.app.Application
import android.content.Context
import com.iliarb.tmdbexplorer.App
import com.iliarb.tmdbexplorer.injection.ApplicationContext
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author Ilya Ryabchuk on 21.10.16.
 */
@Module
class ApplicationModule(val app: Application) {

    @Provides
    @Singleton
    fun provideApplication(): App = app as App

    @Provides
    @ApplicationContext
    fun provideApplicationContext(): Context = app

}