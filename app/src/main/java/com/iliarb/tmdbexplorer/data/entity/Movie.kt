package com.iliarb.tmdbexplorer.data.entity

import com.iliarb.tmdbexplorer.data.network.response.CreditsResponse
import com.iliarb.tmdbexplorer.data.network.response.VideoResponse

/**
 * @author Ilya Ryabchuk on 22.10.16.
 */
data class Movie(
        val id: Int,
        val title: String,
        val overview: String,
        val backdropPath: String,
        val releaseDate: String?,
        val popularity: Float,
        val budget: Long,
        val voteCount: Int,
        val runtime: Int,
        val credits: CreditsResponse,
        val images: Backdrops,
        val productionCompanies: MutableList<Company>,
        val keywords: Keywords,
        val voteAverage: Float,
        val videos: VideoResponse,
        val homepage: String)