package com.iliarb.tmdbexplorer.injection

/**
 * @author Ilya Ryabchuk on 21.10.16.
 */
import javax.inject.Qualifier

@Qualifier
annotation class ApplicationContext