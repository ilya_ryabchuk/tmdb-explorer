package com.iliarb.tmdbexplorer.injection.component

import com.iliarb.tmdbexplorer.injection.module.UiModule
import com.iliarb.tmdbexplorer.injection.scope.UiScope
import com.iliarb.tmdbexplorer.ui.account.AccountActivity
import com.iliarb.tmdbexplorer.ui.auth.AuthActivity
import com.iliarb.tmdbexplorer.ui.details.movie.MovieDetailsActivity
import com.iliarb.tmdbexplorer.ui.main.MainActivity
import com.iliarb.tmdbexplorer.ui.main.movies.MovieListFragment
import com.iliarb.tmdbexplorer.ui.main.tv.TvShowListFragment
import dagger.Subcomponent

/**
 * @author Ilya Ryabchuk on 23.11.16.
 */
@UiScope
@Subcomponent(modules = arrayOf(UiModule::class))
interface UiComponent {

    @Subcomponent.Builder
    interface Builder {

        fun uiModule(module: UiModule): Builder

        fun build(): UiComponent

    }

    fun inject(activity: MainActivity)
    fun inject(activity: AccountActivity)
    fun inject(activity: AuthActivity)
    fun inject(activity: MovieDetailsActivity)

    fun inject(fragment: MovieListFragment)
    fun inject(fragment: TvShowListFragment)

}