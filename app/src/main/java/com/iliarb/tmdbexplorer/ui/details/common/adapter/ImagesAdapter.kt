package com.iliarb.tmdbexplorer.ui.details.common.adapter

import android.support.v7.widget.util.SortedListAdapterCallback
import android.view.ViewGroup
import com.iliarb.tmdbexplorer.R
import com.iliarb.tmdbexplorer.data.entity.Backdrop
import com.iliarb.tmdbexplorer.ui.base.recyclerview.RecyclerViewBaseAdapter
import com.iliarb.tmdbexplorer.ui.details.common.view_holder.ImageViewHolder
import com.iliarb.tmdbexplorer.util.extensions.inflate

/**
 * @author Ilya Ryabchuk on 30.03.17.
 */

class ImagesAdapter(images: MutableList<Backdrop>, val clickListener: (Int) -> Unit)
    : RecyclerViewBaseAdapter<Backdrop, ImageViewHolder>() {

    init {
        initList(Backdrop::class.java, images.size, object : SortedListAdapterCallback<Backdrop>(this) {
            override fun compare(o1: Backdrop?, o2: Backdrop?): Int = 0

            override fun areContentsTheSame(oldItem: Backdrop?, newItem: Backdrop?): Boolean = oldItem?.filePath == newItem?.filePath

            override fun areItemsTheSame(item1: Backdrop?, item2: Backdrop?): Boolean = item1?.filePath == item2?.filePath
        })

        setList(images)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ImageViewHolder {
        return ImageViewHolder(parent!!.inflate(R.layout.item_image), clickListener)
    }

}
