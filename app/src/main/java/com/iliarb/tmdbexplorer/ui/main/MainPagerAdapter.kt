package com.iliarb.tmdbexplorer.ui.main

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.iliarb.tmdbexplorer.ui.main.movies.MovieListFragment
import com.iliarb.tmdbexplorer.ui.main.tv.TvShowListFragment
import javax.inject.Inject

/**
 * @author Ilya Ryabchuk on 22.10.16.
 */
class MainPagerAdapter @Inject constructor(fragmentManager: FragmentManager)
    : FragmentStatePagerAdapter(fragmentManager) {

    companion object {

        private const val TABS_COUNT = 2

    }

    override fun getItem(position: Int): Fragment? {
        return when (position) {
            0 -> MovieListFragment.newInstance()
            1 -> TvShowListFragment.newInstance()
            else -> null
        }
    }

    override fun getCount(): Int = TABS_COUNT
}