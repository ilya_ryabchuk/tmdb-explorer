package com.iliarb.tmdbexplorer.ui.base.recyclerview

import android.support.v7.util.SortedList
import android.support.v7.widget.RecyclerView
import timber.log.Timber

/**
 * @author ilya on 3/11/17.
 */
abstract class RecyclerViewBaseAdapter<M, VH : BaseViewHolder<M>> : RecyclerView.Adapter<VH>() {

    private lateinit var list: SortedList<M>

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list.get(position), position)
    }

    override fun onViewRecycled(holder: VH) {
        super.onViewRecycled(holder)
        holder.prepareForReuse()
    }

    override fun getItemCount(): Int {
        try {
            return list.size()
        } catch (e: UninitializedPropertyAccessException) {
            Timber.e("List is not initialized. Please call initList() first")
            return 0
        }
    }

    fun initList(clazz: Class<M>, initialCapacity: Int = 0, callback: SortedList.Callback<M>) {
        list = SortedList(clazz, callback, initialCapacity)
    }

    fun setList(items: List<M>) {
        if (items.isNotEmpty()) {
            list.clear()
        }
        list.addAll(items)
    }

    fun size() : Int = list.size()

    fun get(position: Int): M = list.get(position)

    fun add(item: M) = list.add(item)

    fun indexOf(item: M) = list.indexOf(item)

    fun updateItemAt(position: Int, item: M) = list.updateItemAt(position, item)

    fun addAll(items: Array<M>) = addAll(items.toList())

    fun remove(item: M) = list.remove(item)

    fun removeItemAt(position: Int): M = list.removeItemAt(position)

    fun beginBatchedUpdated() = list.beginBatchedUpdates()

    fun endBatchedUpdated() = list.endBatchedUpdates()

    fun addAll(items: List<M>) = list.addAll(items)

    fun clear() = list.clear()
}