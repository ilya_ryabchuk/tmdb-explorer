package com.iliarb.tmdbexplorer.data.model.auth

import com.iliarb.tmdbexplorer.data.entity.Session
import com.iliarb.tmdbexplorer.data.network.response.RequestTokenResponse
import com.iliarb.tmdbexplorer.data.network.response.ValidatedTokenResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author Ilya Ryabchuk on 27.11.16.
 */
interface AuthService {

    @GET("authentication/token/new")
    fun createRequestToken(): Single<RequestTokenResponse>

    @GET("authentication/token/validate_with_login")
    fun validateWithLogin(@Query("username") username: String,
                          @Query("password") password: String,
                          @Query("request_token") token: String): Single<ValidatedTokenResponse>

    @GET("authentication/session/new")
    fun authorize(@Query("request_token") token: String): Single<Session>

}