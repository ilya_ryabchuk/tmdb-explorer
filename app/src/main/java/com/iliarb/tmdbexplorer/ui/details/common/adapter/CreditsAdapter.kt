package com.iliarb.tmdbexplorer.ui.details.common.adapter

import android.support.v7.widget.util.SortedListAdapterCallback
import android.view.ViewGroup
import com.iliarb.tmdbexplorer.R
import com.iliarb.tmdbexplorer.data.entity.Cast
import com.iliarb.tmdbexplorer.ui.base.recyclerview.RecyclerViewBaseAdapter
import com.iliarb.tmdbexplorer.ui.details.common.view_holder.CreditViewHolder
import com.iliarb.tmdbexplorer.util.extensions.inflate

/**
 * @author Ilya Ryabchuk on 30.03.17.
 */
class CreditsAdapter(crew: MutableList<Cast>) : RecyclerViewBaseAdapter<Cast, CreditViewHolder>() {

    init {
        initList(Cast::class.java, crew.size, object : SortedListAdapterCallback<Cast>(this) {
            override fun areContentsTheSame(oldItem: Cast?, newItem: Cast?): Boolean {
                return false
            }

            override fun areItemsTheSame(item1: Cast?, item2: Cast?): Boolean {
                return false
            }

            override fun compare(o1: Cast?, o2: Cast?): Int {
                return 0
            }
        })
        setList(crew)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CreditViewHolder {
        return CreditViewHolder(parent!!.inflate(R.layout.item_credit))
    }
}