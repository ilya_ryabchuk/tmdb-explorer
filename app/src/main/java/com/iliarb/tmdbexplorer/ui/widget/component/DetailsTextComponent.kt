package com.iliarb.tmdbexplorer.ui.widget.component

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.iliarb.tmdbexplorer.R
import kotlinx.android.synthetic.main.stub_component_text.view.*

/**
 * @author ilya on 3/19/17.
 */
class DetailsTextComponent @JvmOverloads constructor(context: Context,
                                                     attrs: AttributeSet? = null,
                                                     defStyleAttr: Int = -1)
    : LinearLayout(context, attrs, defStyleAttr) {

    init {
        orientation = VERTICAL

        inflate(context, R.layout.stub_component_text, this)

        if (attrs != null) {
            val attributes = context.obtainStyledAttributes(attrs, R.styleable.DetailsTextComponent)

            val title = attributes.getString(R.styleable.DetailsTextComponent_textComponentTitle)
            if (title != null) {
                componentTitle.text = title
            }

            val text = attributes.getString(R.styleable.DetailsTextComponent_textComponentText)
            if (text != null) {
                componentText.text = text
            }

            attributes.recycle()
        }
    }

    fun setComponentTitle(title: String) {
        componentTitle.text = title
    }

    fun setComponentText(text: String) {
        componentText.text = text
    }
}