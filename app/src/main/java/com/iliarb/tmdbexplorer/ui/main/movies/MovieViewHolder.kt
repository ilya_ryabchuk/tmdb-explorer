package com.iliarb.tmdbexplorer.ui.main.movies

import android.view.View
import com.bumptech.glide.Glide
import com.iliarb.tmdbexplorer.data.entity.Movie
import com.iliarb.tmdbexplorer.ui.base.recyclerview.BaseViewHolder
import com.iliarb.tmdbexplorer.util.DateUtils
import com.iliarb.tmdbexplorer.util.extensions.loadImage
import kotlinx.android.synthetic.main.item_movie.view.*

/**
 * @author Ilya Ryabchuk on 23.10.16.
 */
class MovieViewHolder(itemView: View, private val callback: (Int) -> Unit) : BaseViewHolder<Movie>(itemView) {

    init {
        itemView.setOnClickListener { callback.invoke(adapterPosition) }
    }

    override fun prepareForReuse() = Glide.clear(itemView.itemImage)

    override fun bind(item: Movie, position: Int) {
        itemView.itemImage.loadImage(item.backdropPath)
        itemView.itemTitle.text = item.title
        itemView.itemDate.text = DateUtils.getFormattedDate(item.releaseDate)
    }

}