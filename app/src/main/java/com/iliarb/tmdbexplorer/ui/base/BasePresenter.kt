package com.iliarb.tmdbexplorer.ui.base

import com.iliarb.tmdbexplorer.data.network.ApiException
import com.iliarb.tmdbexplorer.data.network.ErrorHandler
import io.reactivex.SingleTransformer

/**
 * @author ilya on 3/11/17.
 */
abstract class BasePresenter<T : MvpView> : Presenter<T> {

    protected var mvpView: T? = null
        private set

    override fun attachView(mvpView: T) {
        this.mvpView = mvpView
    }

    override fun detachView() {
        mvpView = null
    }

    protected fun <T> applyProgress(): SingleTransformer<T, T> {
        return SingleTransformer {
            it.doOnSubscribe({
                mvpView?.showProgressDialog()
            }).doFinally({
                mvpView?.hideProgressDialog()
            })
        }
    }

    protected val isViewAttached: Boolean
        get() = mvpView != null

    protected val defaultErrorConsumer: ErrorHandler by lazy {
        ErrorHandler(mvpView, true, { throwable, _ ->
            if (throwable is ApiException) {
                mvpView?.showMessage(throwable.message)
            }
        })
    }
}