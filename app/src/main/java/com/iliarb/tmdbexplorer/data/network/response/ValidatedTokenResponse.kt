package com.iliarb.tmdbexplorer.data.network.response

/**
 * @author Ilya Ryabchuk on 27.11.16.
 */
data class ValidatedTokenResponse(val success: Boolean, val requestToken: String)