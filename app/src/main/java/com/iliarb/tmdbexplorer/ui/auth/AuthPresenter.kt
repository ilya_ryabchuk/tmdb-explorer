package com.iliarb.tmdbexplorer.ui.auth

import com.iliarb.tmdbexplorer.data.model.auth.AuthModel
import com.iliarb.tmdbexplorer.ui.base.BasePresenter
import io.reactivex.functions.Consumer
import javax.inject.Inject

/**
 * @author Ilya Ryabchuk on 27.11.16.
 */
class AuthPresenter @Inject constructor(model: AuthModel) : BasePresenter<AuthView>() {

    private val authModel: AuthModel = model

    internal var requestToken: String? = null

    internal fun createRequestToken() {
        authModel.createRequestToken().subscribe(Consumer { requestToken = it.requestToken }, defaultErrorConsumer)
    }

    internal fun validateWithLogin(username: String, password: String) {
        if (requestToken != null) {
            authModel.authorize(requestToken as String, username, password).subscribe(Consumer {

                mvpView?.onLoginSuccess()

            }, defaultErrorConsumer)
        }
    }
}