package com.iliarb.tmdbexplorer.data.network.response

/**
 * @author Ilya Ryabchuk on 05.12.16.
 */
data class PlacesResponse(val results: MutableList<Place>) {

    data class Place(val id: String, val name: String, val geometry: Geometry)

    data class Geometry(val location: Location)

    data class Location(val lat: Double, val lng: Double)

}