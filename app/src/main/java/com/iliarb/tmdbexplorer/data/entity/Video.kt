package com.iliarb.tmdbexplorer.data.entity

/**
 * @author Ilya Ryabchuk on 01.11.16.
 */
data class Video(
        val id: String,
        val site: String,
        val name: String,
        val type: String,
        val key: String)