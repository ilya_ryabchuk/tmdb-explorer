package com.iliarb.tmdbexplorer.data.entity

import com.iliarb.tmdbexplorer.data.network.response.CreditsResponse

/**
 * @author Ilya Ryabchuk on 06.11.16.
 */
data class TvShow(
        val id: Int,
        val backdropPath: String,
        val createdBy: MutableList<Author>,
        val firstAirDate: String,
        val lastAirDate: String?,
        val homepage: String,
        val name: String,
        val images: Backdrops,
        val numberOfEpisodes: Int,
        val numberOfSeasons: Int,
        val credits: CreditsResponse,
        val popularity: Float,
        val overview: String,
        val productionCompanies: MutableList<Company>,
        val seasons: MutableList<Season>,
        val status: String,
        val voteAverage: Float,
        val voteCount: Int)