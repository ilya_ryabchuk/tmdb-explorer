package com.iliarb.tmdbexplorer.injection.component

import android.content.Context
import com.iliarb.tmdbexplorer.injection.ApplicationContext
import com.iliarb.tmdbexplorer.injection.module.ApplicationModule
import com.iliarb.tmdbexplorer.injection.module.NetworkModule
import com.iliarb.tmdbexplorer.injection.module.UtilsModule
import com.iliarb.tmdbexplorer.util.rx.RxSharedPreferences
import dagger.Component
import javax.inject.Singleton

/**
 * @author Ilya Ryabchuk on 21.10.16.
 */
@Singleton
@Component(modules = arrayOf(ApplicationModule::class, NetworkModule::class, UtilsModule::class))
interface ApplicationComponent {

    @ApplicationContext
    fun context() : Context
    fun preferences() : RxSharedPreferences

    fun uiComponentBuilder() : UiComponent.Builder

}