package com.iliarb.tmdbexplorer.ui.base

import android.support.v7.app.AppCompatActivity
import com.iliarb.tmdbexplorer.ui.widget.dialog.MessageDialog

/**
 * @author ilya on 3/11/17.
 */
abstract class BaseActivity : AppCompatActivity(), MvpView {

    override fun showMessage(messageRes: Int, errorCode: Int) {
        showMessage(getString(messageRes), errorCode)
    }

    override fun showMessage(message: String, errorCode: Int) {
        MessageDialog.newInstance(message, null).show(supportFragmentManager)
    }

    override fun showProgressDialog() {

    }

    override fun hideProgressDialog() {

    }
}