package com.iliarb.tmdbexplorer.data.entity

/**
 * @author Ilya Ryabchuk on 29.11.16.
 */
data class SearchResult(
        val id: Int,
        val backdropPath: String,
        val title: String?,
        val name: String?,
        val releaseDate: String?,
        val firstAirDate: String?,
        val lastAirDate: String?)