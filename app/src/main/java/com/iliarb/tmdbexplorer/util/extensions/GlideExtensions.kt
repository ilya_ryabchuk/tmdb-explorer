package com.iliarb.tmdbexplorer.util.extensions

import android.support.annotation.DrawableRes
import android.support.annotation.Px
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.iliarb.tmdbexplorer.BuildConfig
import com.iliarb.tmdbexplorer.data.network.ApiConfig
import timber.log.Timber

/**
 * @author Ilya Ryabchuk on 17.03.17.
 */

private const val NO_OVERRIDE = -1

fun ImageView.loadImage(url: String,
                        @Px width: Int = NO_OVERRIDE,
                        @Px height: Int = NO_OVERRIDE,
                        @DrawableRes errorDrawable: Int = 0,
                        @DrawableRes placeholderDrawable: Int = 0,
                        centerCrop: Boolean = true,
                        animate: Boolean = false,
                        vararg transformations: BitmapTransformation = emptyArray()) {

    val request = Glide.with(context).load("${ApiConfig.IMAGE_URL}/$url")

    request.error(errorDrawable)
    request.placeholder(placeholderDrawable)

    if (transformations.isNotEmpty()) {
        request.transform(*transformations)
    }

    if (centerCrop) {
        request.centerCrop()
    }

    if (!animate) {
        request.dontAnimate()
    }

    if (width != NO_OVERRIDE && height != NO_OVERRIDE) {
        request.override(width, height)
    }

    if (BuildConfig.DEBUG) {
        request.listener(object : RequestListener<Any, GlideDrawable> {
            override fun onException(e: Exception?, model: Any?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
                Timber.d("onException() called with: e = [$e], model = [$model], target = [$target], isFirstResource = [$isFirstResource]")
                return false
            }

            override fun onResourceReady(resource: GlideDrawable?, model: Any?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                Timber.d("onResourceReady() called with: resource = [$resource], model = [$model], target = [$target], isFromMemoryCache = [$isFromMemoryCache], isFirstResource = [$isFirstResource]")
                return false
            }

        })
    }

    request.into(this)
}