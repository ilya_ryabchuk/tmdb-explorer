package com.iliarb.tmdbexplorer.data.network

/**
 * @author ilya on 3/14/17.
 */
class ApiException(override val message: String, val code: Int) : Throwable(message)