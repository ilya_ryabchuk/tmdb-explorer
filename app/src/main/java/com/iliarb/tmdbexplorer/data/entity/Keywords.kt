package com.iliarb.tmdbexplorer.data.entity

/**
 * @author Ilya Ryabchuk on 24.10.16.
 */
data class Keywords(val keywords: MutableList<Keyword>)