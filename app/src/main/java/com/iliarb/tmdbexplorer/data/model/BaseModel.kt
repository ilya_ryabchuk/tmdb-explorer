package com.iliarb.tmdbexplorer.data.model

import com.google.gson.Gson
import com.iliarb.tmdbexplorer.data.network.ApiException
import com.iliarb.tmdbexplorer.data.network.response.ErrorBody
import io.reactivex.Completable
import io.reactivex.CompletableTransformer
import io.reactivex.Single
import io.reactivex.SingleTransformer
import retrofit2.HttpException

abstract class BaseModel {

    private val gson: Gson = Gson()

    fun <T> transformException(): SingleTransformer<T, T> {
        return SingleTransformer { it.onErrorResumeNext({ throwable -> parseHttpException(throwable) }) }
    }

    fun completableTransformException(): CompletableTransformer {
        return CompletableTransformer { it.onErrorResumeNext({ it -> parseCompletableHttpException(it) }) }
    }

    private fun <T> parseHttpException(throwable: Throwable): Single<T> {
        if (throwable !is HttpException) {
            return Single.error(throwable)
        }

        val error = parseHttpError(throwable)
        if (error != null && !error.success) {
            return Single.error(ApiException(error.statusMessage, error.statusCode))
        }

        return Single.error(throwable)
    }

    private fun parseCompletableHttpException(throwable: Throwable): Completable {
        if (throwable !is HttpException) {
            return Completable.error(throwable)
        }

        val error = parseHttpError(throwable)
        if (error != null && !error.success) {
            return Completable.error(ApiException(error.statusMessage, error.statusCode))
        }

        return Completable.error(throwable)
    }

    private fun parseHttpError(throwable: Throwable): ErrorBody? {
        val body = (throwable as HttpException).response().errorBody()
        return gson.fromJson(body.string(), ErrorBody::class.java)
    }

}