package com.iliarb.tmdbexplorer.data.entity

import com.google.gson.annotations.Expose

/**
 * @author Ilya Ryabchuk on 27.11.16.
 */
data class Genre(val id: Int, val name: String, @Expose var checked: Boolean = false)