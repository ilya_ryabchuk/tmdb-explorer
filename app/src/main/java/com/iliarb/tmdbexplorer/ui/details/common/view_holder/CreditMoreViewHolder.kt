package com.iliarb.tmdbexplorer.ui.details.common.view_holder

import android.view.View
import com.iliarb.tmdbexplorer.ui.base.recyclerview.BaseViewHolder

/**
* @author ilya.ryabchuk on 03.04.2017.
*/
class CreditMoreViewHolder(view: View, onItemClick: () -> Unit) : BaseViewHolder<Any>(view) {

    init {
        itemView.setOnClickListener {
            onItemClick.invoke()
        }
    }

    override fun prepareForReuse() {

    }

    override fun bind(item: Any, position: Int) {

    }
}